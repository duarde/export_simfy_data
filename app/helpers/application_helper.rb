module ApplicationHelper
  def logged_in?
    current_session.access_token.present?
  end
end
