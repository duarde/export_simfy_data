class SessionController < ApplicationController
  def login
  end

  def create
    simfy = Simfy.create_and_login(params[:login][:username], params[:login][:password])
    session[:username] = params[:login][:username]
    session[:access_token] = simfy.access_token
    redirect_to root_path
  end

  def logout
    reset_session
    redirect_to login_path
  end
end