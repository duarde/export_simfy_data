class HomeController < ApplicationController
  def index
  end

  def artists
    render :json => simfy.artists, :layout => false
  end
end