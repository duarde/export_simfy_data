require 'ostruct'

class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_session

  def current_session
    OpenStruct.new(session)
  end

  private
  
  def simfy
    Simfy.create_from_access_token(current_session.access_token)
  end

end
