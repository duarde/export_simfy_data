# Simfy service
# Retrieve favorites and playlists.

class Simfy
  attr_reader :username, :password, :access_token

  class << self
    def create_and_login(username, password)
      simfy = new(nil)
      simfy.get_access_token(username, password)
      simfy
    end

    def create_from_access_token(access_token)
      new(access_token)
    end
  end

  def initialize(access_token)
    @access_token = access_token
  end

  def logged_in?
    !!access_token
  end

  def get_access_token(username, password)
    @access_token = SimfyAPI.get_access_token(username, password)
  end

  def artists
    artist_ids = SimfyAPI.artist_ids(access_token)
    artists = artist_ids.map do |artist_id|
      SimfyAPI.artist_details(artist_id, access_token)
    end
  end
end