require 'faraday'
require 'multi_json'
 
class SimfyAPI
  class ParseError < Exception;end
 
  class << self
    def connection
      Faraday.new(:url => "https://www.simfy.de") do |faraday|
        faraday.request  :url_encoded             # form-encode POST params
        # faraday.response :logger                  # log requests to STDOUT
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      end
    end
 
    def get_data(path, params = {})
      connection.get "/api/#{path}.json", params
    end
 
    def post_data(path, params = {})
      connection.post "/api/#{path}.json", params
    end
 
    def get_access_token(username, password)
      params = {
        "user_session[email]" => username,
        "user_session[password]" => password,
        "device_or_application"  => 'desktop',
        "device_or_application_version" => "180",
      }
      # debugger
      response = post_data("get_single_access_token", params)
      json = MultiJson.load(response.body)
      json["access_token"]
 
    rescue MultiJson::DecodeError
      raise ParseError, "Could not parse response from simfy: \n#{response.body}"
    end
 
    def artist_ids(access_token)
      response = get_data("likings", :access_token => access_token, :type => "artists")
      json = MultiJson.load(response.body)
      json["results"].collect do |artist_hash|
        artist_hash["likeable_id"]
      end
    end

    def artist_details(artist_id, access_token)
      response = get_data("artists/#{artist_id}", :access_token => access_token)
      json = MultiJson.load(response.body)
      json["artist"]
    end

    def all_favorites(access_token)
      response = get_data("all_likings", :access_token => access_token, :type => "artists")
      json = MultiJson.load(response.body)
      json["results"]
    end
  end
end