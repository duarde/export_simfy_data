require_relative '../app/services/simfy_api'
require_relative 'vcr_spec_helper'
 
# :record => :new_episodes
 
describe SimfyAPI do
  describe "#get_access_token" do
    it "should return an access_token" do
      VCR.use_cassette('bobs_simfy') do
        access_token = SimfyAPI.get_access_token('bob23', 'bob23 password')
        access_token.should_not be_nil
      end
    end
 
    it "should raise error if could not get access_token" do
      VCR.use_cassette('bobs_simfy') do
        lambda{ SimfyAPI.get_access_token('wrong', 'credentials')}.should raise_error(SimfyAPI::ParseError)
      end
    end
  end
 
  describe "#artist_ids" do
    it "should return artist_ids" do
      VCR.use_cassette('bobs_simfy') do
        SimfyAPI.artist_ids('c1GdGoML7hU871WzkB6').should == [115304, 129578]
      end
    end
  end
end