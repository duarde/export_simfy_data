require 'vcr'
 
VCR.configure do |c|
  c.cassette_library_dir = 'fixtures/vcr_cassettes'
  c.hook_into :webmock # or :fakeweb
  c.allow_http_connections_when_no_cassette = true
  c.default_cassette_options = { 
    # :decode_compressed_response => true,
    :match_requests_on => [:uri, :path, :body],
    :serialize_with => :syck,
  }
end