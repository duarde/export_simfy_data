require_relative '../app/services/simfy'
require_relative '../app/services/simfy_api'
 
describe Simfy do
 
  describe "#create_and_login" do
    it "should be logged in" do
      SimfyAPI.stub(:get_access_token).and_return("access_token")
      simfy = Simfy.create_and_login('bob', 'bob23 password')
      simfy.should be_logged_in
    end
  end
 
  describe "#create_from_access_token" do
    it "should return a simfy instance" do
      simfy = Simfy.create_from_access_token('access_token')
      simfy.should be_logged_in
    end
  end
 
  describe "#artists" do
    it "should return artists from simfy api" do
      simfy = Simfy.new("name", "pass")
      simfy.stub(:access_token).and_return "access_token"
      SimfyAPI.should_receive(:artist_ids).with("access_token").and_return ['artist_id_1']
      SimfyAPI.should_receive(:artist_details).with('artist_id_1', "access_token").and_return "artist_details"
      simfy.artists.should == ["artist_details"]
    end
  end
end