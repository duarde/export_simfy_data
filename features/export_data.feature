Feature: Export data from simfy
  In order to have my data
  As a user
  I want to export it

  Scenario: Export artists
    Given I sign in as "bob"
    When I download the artists
    Then I see a list of my favorite artists
  
