require './spec/vcr_spec_helper'

Given /^I sign in as "bob"$/ do
  VCR.use_cassette('bobs_simfy', :match_requests_on => [:uri, :path, :body]) do
    # @simfy = Simfy.create_and_login('bob23', 'bob23 password')
    visit login_path
    fill_in 'login_username', :with => "bob"
    fill_in 'login_password', :with => "password"
    click_button "Login"

    current_path.should == root_path
    page.body.should include("Logged in as bob")
    page.body.should include("Artists")
  end
end