require './spec/vcr_spec_helper'

When /^I download the artists$/ do
  click_link "Artists"
end

Then /^I see a list of my favorite artists$/ do
  expected_json = [{"type"=>"Person", "image_120x120"=>"http://www.simfy.de/photos/artists/115304/120.jpg", "updated_at"=>"2010-08-09T20:45:41+02:00", "biography_gnu"=>"", "image_160x160"=>"http://www.simfy.de/photos/artists/115304/160.jpg", "biography_url"=>nil, "image_546x248"=>"http://www.simfy.de/photos/artists/115304/546x248.jpg", "image_44x44"=>"http://www.simfy.de/photos/artists/115304/44.jpg", "image_320x100"=>"http://www.simfy.de/photos/artists/115304/320x100.jpg", "name"=>"Dapayk & Padberg", "id"=>115304, "image_100x100"=>"http://www.simfy.de/photos/artists/115304/100.jpg", "albums_count"=>16, "short_biography"=>""}, {"type"=>"Person", "albums_count"=>18, "biography_url"=>nil, "updated_at"=>"2010-10-23T08:24:29+02:00", "image_546x248"=>"http://www.simfy.de/photos/artists/129578/546x248.jpg", "image_44x44"=>"http://www.simfy.de/photos/artists/129578/44.jpg", "image_320x100"=>"http://www.simfy.de/photos/artists/129578/320x100.jpg", "image_100x100"=>"http://www.simfy.de/photos/artists/129578/100.jpg", "short_biography"=>"", "name"=>"Solomun", "image_120x120"=>"http://www.simfy.de/photos/artists/129578/120.jpg", "id"=>129578, "biography_gnu"=>"", "image_160x160"=>"http://www.simfy.de/photos/artists/129578/160.jpg"}]
   VCR.use_cassette('bobs_simfy') do
    # @simfy.artists.should == expected_json
    page.body.should == expected_json
  end
end